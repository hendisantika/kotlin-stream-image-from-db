package com.hendisantika.kotlinstreamimagefromdb.entity

import org.springframework.web.multipart.MultipartFile
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Lob
import javax.xml.bind.DatatypeConverter

/**
 * Created by IntelliJ IDEA.
 * Project : kotlin-stream-image-from-db
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-28
 * Time: 07:34
 */
@Entity
data class PersistedImage(@field: Id @field: GeneratedValue var id: Long = 0,
        //The bytes field needs to be marked as @Lob for Large Object Binary
                          @field: Lob var bytes: ByteArray? = null,
                          var mime: String? = "") {

    fun toStreamingURI(): String {
        //We need to encode the byte array into a base64 String for the browser
        val base64 = DatatypeConverter.printBase64Binary(bytes)

        //Now just return a data string. The Browser will know what to do with it
        return "data:$mime;base64,$base64"
    }
}

//This is a Kotlin extension function that turns a MultipartFile into a PersistedImage
fun MultipartFile.toPersistedImage() = PersistedImage(bytes = this.bytes, mime = this.contentType)