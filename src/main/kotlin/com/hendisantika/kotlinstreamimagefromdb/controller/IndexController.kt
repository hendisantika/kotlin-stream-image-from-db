package com.hendisantika.kotlinstreamimagefromdb.controller

import com.hendisantika.kotlinstreamimagefromdb.entity.toPersistedImage
import com.hendisantika.kotlinstreamimagefromdb.service.ImageService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestPart
import org.springframework.web.multipart.MultipartFile

/**
 * Created by IntelliJ IDEA.
 * Project : kotlin-stream-image-from-db
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-28
 * Time: 07:38
 */
@Controller
@RequestMapping("/")
class IndexController(@Autowired private val imageService: ImageService) {

    @RequestMapping(method = arrayOf(RequestMethod.GET))
    fun doGet(model: Model): String {
        model.addAttribute("images", imageService.loadAll())
        return "index"
    }

    @RequestMapping(method = arrayOf(RequestMethod.POST))
    fun doPost(
            //Grab the uploaded image from the form
            @RequestPart("image") multiPartFile: MultipartFile,
            model: Model): String {
        //Save the image file
        imageService.save(multiPartFile.toPersistedImage())
        model.addAttribute("images", imageService.loadAll())
        return "index"
    }
}