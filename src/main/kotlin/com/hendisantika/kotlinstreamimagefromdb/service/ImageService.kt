package com.hendisantika.kotlinstreamimagefromdb.service

import com.hendisantika.kotlinstreamimagefromdb.entity.PersistedImage
import com.hendisantika.kotlinstreamimagefromdb.repository.ImageRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 * Created by IntelliJ IDEA.
 * Project : kotlin-stream-image-from-db
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-28
 * Time: 07:37
 */
@Transactional
@Service
class ImageService(@Autowired private val imageRepository: ImageRepository) {

    fun save(persistedImage: PersistedImage) {
        imageRepository.save(persistedImage)
    }

    fun loadAll() = imageRepository.loadAll()
}