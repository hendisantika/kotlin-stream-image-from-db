package com.hendisantika.kotlinstreamimagefromdb.config

import org.hibernate.SessionFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import javax.persistence.EntityManagerFactory

/**
 * Created by IntelliJ IDEA.
 * Project : kotlin-stream-image-from-db
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-28
 * Time: 07:36
 */
@Configuration
class DataConfig {

    @Bean
    fun sessionFactory(@Autowired entityManagerFactory: EntityManagerFactory):
            SessionFactory = entityManagerFactory.unwrap(SessionFactory::class.java)
}