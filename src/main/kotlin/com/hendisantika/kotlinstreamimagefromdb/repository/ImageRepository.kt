package com.hendisantika.kotlinstreamimagefromdb.repository

import com.hendisantika.kotlinstreamimagefromdb.entity.PersistedImage
import org.hibernate.SessionFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

/**
 * Created by IntelliJ IDEA.
 * Project : kotlin-stream-image-from-db
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-28
 * Time: 07:36
 */
@Repository
class ImageRepository(@Autowired private val sessionFactory: SessionFactory) {

    fun save(persistedImage: PersistedImage) {
        sessionFactory.currentSession.saveOrUpdate(persistedImage)
    }

    fun loadAll() = sessionFactory.currentSession.createCriteria(PersistedImage::class.java).list() as List<PersistedImage>
}