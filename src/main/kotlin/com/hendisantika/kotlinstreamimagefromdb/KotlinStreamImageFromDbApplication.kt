package com.hendisantika.kotlinstreamimagefromdb

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KotlinStreamImageFromDbApplication

fun main(args: Array<String>) {
    runApplication<KotlinStreamImageFromDbApplication>(*args)
}
